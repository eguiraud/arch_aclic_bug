#!/bin/bash

cd /build_root
ctest -V -R "(roottest-root-dataframe-ctors|roottest-root-dataframe-test_snapshot_manytasks|roottest-root-dataframe-test_reduce)"  -- -j8

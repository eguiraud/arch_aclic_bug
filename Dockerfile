FROM archlinux

COPY packages packages
RUN pacman -Syu --noconfirm --quiet $(cat packages)
RUN git clone https://github.com/root-project/root.git root_src &&\
    mkdir build_root &&\
    cd build_root &&\
    cmake -DLLVM_BUILD_TYPE=Debug -DCMAKE_BUILD_TYPE=Debug -Dtesting=ON -Droottest=ON -Droofit=OFF -Dtmva=OFF ../root_src &&\
    cmake --build . -- -j8
COPY repro.sh repro.sh

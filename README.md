# Reproduce issue with ACLiC on Arch Linux

```bash
$ docker build -t aclic_bug .
$ docker run -it aclic_bug
# bash repro.sh
```

The issue has been reported at https://github.com/root-project/root/issues/7366 .
